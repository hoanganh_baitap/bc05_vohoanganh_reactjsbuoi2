import React, { Component } from "react";
// import data kính
import dataGlasses from "../Data/dataGlasses.json";

export default class ThuKinh extends Component {
  // state
  state = {
    kinhHienTai: {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./glassesImage/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  };

  // render danh sách kính
  renderGlasses = () => {
    return dataGlasses.map((glasses, index) => {
      // return danh sach kinh ra giao dien, gắn sự kiện onclick khi người dùng click vào gọi hàm đổi kính
      return (
        <img
          key={index}
          onClick={() => {
            this.doiKinh(glasses);
          }}
          src={glasses.url}
          alt=""
          style={{ width: "120px", padding: "20px", cursor: "pointer" }}
        />
      );
    });
  };

  // tạo hàm đổi kính
  doiKinh = (newGlasses) => {
    this.setState({
      kinhHienTai: newGlasses,
    });
  };
  render() {
    return (
      <div className="container">
        <h1 className="text-center p-5 text-primary">THAY KÍNH CHO MODEL</h1>
        <div className="text-center position-relative">
          <div>
            <img
            className="position-absolute"
            style={{ width: "400px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <img
            className="position-absolute"
            src={this.state.kinhHienTai.url}
            alt=""
            style={{ width: "200px", top: "130px", left: "655px" }}
          />
          </div>
          <div className="position-absolute" style={{left: "-20px"}}>
            <p>Tên kính: {this.state.kinhHienTai.name}</p>
            <p>Mô tả: {this.state.kinhHienTai.desc}</p>
          </div>

        </div>
        <div style={{ marginTop: "600px" }}>
          {/* nơi hiển thị danh sách kính */}
          <h2>Chọn Kính</h2>
          {this.renderGlasses()}
        </div>
      </div>
    );
  }
}
